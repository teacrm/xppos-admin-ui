import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { LayuiVueResolver } from "unplugin-vue-components/resolvers";
import { resolve } from "path";

const excludeComponents = ["LightIcon", "DarkIcon"];

export default defineConfig({
  base: "/erp/", //项目打包运行目录-WWWW网站服务器二级目录
  resolve: {
    alias: {
      "@": resolve(__dirname, "./src"), //资源目录
    },
  },
  plugins: [
    AutoImport({
      resolvers: [LayuiVueResolver()],
    }),
    Components({
      resolvers: [
        LayuiVueResolver({
          resolveIcons: true,
          exclude: excludeComponents,
        }),
      ],
    }),
    vue(),
  ],
  server: {
    host: "127.0.0.1",
    // host: '0.0.0.0',
    // hmr: true, //热更新
    port: 8081, //指定开发服务器端口：默认3000
    // https: false,
    // host: true, // 监听所有地址
    open: true, //启动时自动在浏览器中打开
    // cors: false, //为开发服务器配置 CORS
    proxy: {
      "/api": {
        // "/api"代理匹配目录要和在axios里面的baseURL: "/api",一样
        target: "https://sp.6-7-8-9.com/admin1", //调用远程API接口调试
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },

  build: {
    // target: "modules", //设置最终构建的浏览器兼容目标  //es2015(编译成es5) | modules
    // outDir: "D:/wwwroot/127.0.0.1/tp6/public/q3", // 构建包名  默认：dist
    outDir: "dist", // 构建得包名  默认：dist
    assetsDir: "assets", // 静态资源得存放路径文件名  assets
    emptyOutDir: true, // 构建目录自动清除
    // sourcemap: false, //构建后是否生成 source map 文件
    cssTarget: "chrome61", //防止 vite 将 rgba() 颜色转化为 #RGBA 十六进制符号的形式
    // brotliSize: false, // 启用/禁用 brotli 压缩大小报告。 禁用该功能可能会提高大型项目的构建性能
    chunkSizeWarningLimit: 1000, //chunk 大小警告的限制（以 kbs 为单位）默认：500
    rollupOptions: {
      output: {
        // 最小化拆分包
        manualChunks(id) {
          if (id.includes("node_modules")) {
            return id
              .toString()
              .split("node_modules/")[1]
              .split("/")[0]
              .toString();
          }
        },
        // js,css,img分目录
        chunkFileNames: "assets/js/[name].[hash].js", // 用于命名代码拆分时创建的共享块的输出命名，[name]表示文件名,[hash]表示该文件内容hash值
        entryFileNames: "assets/js/[name]-[hash].js",
        assetFileNames: "assets/[ext]/[name]-[hash].[ext]",
      },
    },

    minify: "terser", // 混淆器,terser 构建后文件体积更小，项目压缩 :boolean | 'terser' | 'esbuild'
    terserOptions: {
      // 生产环境移除console// 要设置-混淆器-minify: "terser", // 混淆器,
      compress: {
        drop_console: true,
        drop_debugger: true,
      },
      output: {
        comments: true, // 去掉注释内容
      },
    },
  },
});
