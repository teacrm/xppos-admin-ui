// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
class PNDB {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };

  // 表格列配置
  columns = [
    { title: "行号", width: "50px", type: "number" },
    {
      title: "录入码",
      width: "160px",
      key: "in_code",
      customSlot: "in_code",
      align: "center",
    },
    {
      title: "条码",
      width: "150px",
      key: "barcode",
      customSlot: "barcode",
      align: "center",
    },
    {
      title: "名称",
      width: "150px",
      key: "goods_name",
      customSlot: "goods_name",
      align: "center",
    },
    {
      title: "单位",
      width: "60px",
      key: "unit",
      customSlot: "unit",
      align: "center",
    },
    {
      title: "大单位",
      width: "57px",
      key: "unit_pack",
      customSlot: "unit_pack",
      align: "center",
    },
    {
      title: "数量",
      width: "70px",
      key: "number",
      customSlot: "number",
      align: "center",
    },
    {
      title: "基本数量",
      width: "70px",
      key: "number_unit",
      customSlot: "number_unit",
      align: "center",
    },
    {
      title: "实际进价",
      width: "70px",
      key: "prc_in_actual",
      customSlot: "prc_in_actual",
      align: "center",
    },
    {
      title: "档案进价",
      width: "70px",
      key: "prc_in",
      customSlot: "prc_in",
      align: "center",
    },
    {
      title: "备注",
      minWidth: "100px",
      key: "remark",
      customSlot: "remark",
      align: "center",
    },
    {
      title: "系统编码",
      width: "210px",
      key: "goods_dbid",
      customSlot: "goods_dbid",
      align: "center",
    },
    {
      title: "结构ID",
      width: "60px",
      key: "id",
      customSlot: "id",
      align: "center",
    },
    {
      title: "操作",
      width: "50px",
      customSlot: "operator",
      key: "operator",
      align: "center",
      fixed: "right",
    },
  ];

  // 表格列配置-历史单据列表
  Table_list_header_columns = [
    { title: "行号", width: "50px", type: "number" },
    {
      title: "操作",
      width: "50px",
      customSlot: "operator",
      key: "operator",
      align: "center",
      fixed: "left",
    },
    {
      title: "单据状态",
      width: "70px",
      key: "state",
      customSlot: "state",
      align: "center",
    },
    {
      title: "单据类型",
      width: "70px",
      key: "type",
      customSlot: "type",
      align: "center",
    },

    {
      title: "供应厂商",
      width: "140px",
      key: "sup_dbid",
      customSlot: "sup_dbid",
      align: "center",
    },
    {
      title: "创建时间",
      width: "140px",
      key: "time_new",
      customSlot: "time_new",
      align: "center",
    },
    {
      title: "修改时间",
      width: "140px",
      key: "time_up",
      customSlot: "time_up",
      align: "center",
    },
    {
      title: "系统编码",
      width: "150px",
      key: "dbid",
      customSlot: "dbid",
      align: "center",
    },
    {
      title: "长度",
      width: "50px",
      key: "detail_length_insert",
      customSlot: "detail_length_insert",
      align: "center",
    },
    {
      title: "备注",
      key: "remark",
      customSlot: "remark",
      align: "center",
    },
  ];
  // 表格列配置-历史单据列表-明细
  Table_list_detail_columns = [
    { title: "行号", width: "50px", type: "number" },
    {
      title: "商品名称",
      width: "160px",
      key: "goods_name",
      customSlot: "goods_name",
      align: "center",
    },
    {
      title: "单据编号",
      width: "210px",
      key: "order_dbid",
      customSlot: "order_dbid",
      align: "center",
    },
    {
      title: "商品编号",
      width: "210px",
      key: "goods_dbid",
      customSlot: "goods_dbid",
      align: "center",
    },
  ];
}
export default PNDB;
