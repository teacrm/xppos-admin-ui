// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
class PNDB {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };


  // 表格列配置
  columns = [
    { title: "序号", width: "60px", type: "number" },
    // {
    //   title: "行号",
    //   width: "60px",
    //   customSlot: "index2",
    //   key: "index2",
    //   align: "center",
    // },
    {
      title: "主键",
      width: "80px",
      customSlot: "id",
      key: "id",
      sort: "desc",
      align: "center",
    },

    {
      title: "数据编号",
      width: "250px",
      customSlot: "goods_dbid",
      key: "goods_dbid",
      sort: "desc",
      align: "center",
    },
    {
      title: "数量",
      width: "70px",
      customSlot: "number",
      key: "number",
      sort: "desc",
      align: "center",
    },
  
  ];
}
export default PNDB;
