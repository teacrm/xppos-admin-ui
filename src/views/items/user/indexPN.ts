// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
class PNDB {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };

  // 用户状态_A正常B停用
  state = (res: string) => {
    if (res == "A") {
      return "正常";
    } else if (res == "B") {
      return '<span style="color: #FF6C37;">停用</span>';
    } else {
      return "溢出";
    }
  };
  // 用户类型_ADMIN-AGENT-VIP-WX-USER-POS
  type = (res: string) => {
    if (res == "USER") {
      return "普通";
    } else if (res == "POS") {
      return "收银";
    } else if (res == "ADMIN") {
      return "管理";
    } else if (res == "AGENT") {
      return "代理";
    } else {
      return "溢出";
    }
  };
  // 表格列配置
  columns = [
    { title: "序号", width: "60px", type: "number" },
    {
      title: "行号",
      width: "60px",
      customSlot: "index2",
      key: "index2",
      align: "center",
    },
    {
      title: "主键",
      width: "80px",
      customSlot: "id",
      key: "id",
      sort: "desc",
      align: "center",
    },

    {
      title: "名称",
      width: "210px",
      key: "name",
      sort: "desc",
      align: "center",
    },
    {
      title: "类型",
      width: "80px",
      customSlot: "type",
      key: "type",
      sort: "desc",
      align: "center",
    },
    {
      title: "状态",
      width: "80px",
      customSlot: "state",
      key: "u_state",
      sort: "desc",
      align: "center",
    },
    {
      title: "数据编号",
      width: "250px",
      customSlot: "dbid",
      key: "dbid",
      sort: "desc",
      align: "center",
    },

    {
      title: "操作",
      width: "54px",
      customSlot: "operator",
      key: "operator",
      fixed: "right",
    },
  ];
}
export default PNDB;
