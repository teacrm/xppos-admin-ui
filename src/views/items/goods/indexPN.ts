// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
class PNDB {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };

  // 商品状态_A正常B停用C只销
  state = (res: string) => {
    if (res == "A") {
      return "正常";
    } else if (res == "B") {
      return '<span style="color: #FF6C37;">停用</span>';
    } else if (res == "C") {
      return "只销";
    } else {
      return "溢出";
    }
  };
  unit_pack = (res: string) => {
    if (res == "A") {
      return "是";
    } else if (res == "B") {
      return '<span style="color: #FF6C37;">否</span>';
    } else {
      return "溢出";
    }
  };
  // 表格列配置
  columns = [
    { title: "序号", width: "60px", type: "number" },
    {
      title: "行号",
      width: "60px",
      customSlot: "index2",
      key: "index2",
      align: "center",
    },
    {
      title: "主键",
      width: "80px",
      customSlot: "id",
      key: "id",
      sort: "desc",
      align: "center",
    },
    {
      title: "主供货商",
      width: "100px",
      key: "sup_dbid",
      sort: "desc",
      align: "center",
    },

    {
      title: "名称",
      width: "210px",
      key: "name",
      sort: "desc",
      align: "center",
    },
    {
      title: "大包",
      width: "66px",
      customSlot: "unit_pack",
      key: "unit_pack",
      sort: "desc",
      align: "center",
    },
    {
      title: "类别ID",
      width: "60px",
      key: "cls_id",
      align: "center",
    },
    {
      title: "类别",
      width: "100px",
      key: "cls_name",
      align: "center",
    },
    {
      title: "商品状态",
      width: "100px",
      customSlot: "state",
      key: "state",
      sort: "desc",
      align: "center",
    },
    {
      title: "操作",
      width: "54px",
      customSlot: "operator",
      key: "operator",
      fixed: "right",
    },
  ];
}
export default PNDB;
