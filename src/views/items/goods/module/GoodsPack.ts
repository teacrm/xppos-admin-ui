// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
// 多单位/多条码/多价格
class GoodsPack {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };

  // 商品状态_A正常B停用C只销
  state = (res: string) => {
    if (res == "A") {
      return "正常";
    } else if (res == "B") {
      return '<span style="color: #FF6C37;">停用</span>';
    } else if (res == "C") {
      return "只销";
    } else {
      return "溢出";
    }
  };


  
  db_state = (res: string) => {
    if (res == "A") {
      return "正常";
    } else if (res == "B") {
      return '<span style="color: #FF6C37;">停用</span>';
    } else if (res == "C") {
      return "只读";
    } else if (res == "D") {
      return "回收";
    } else if (res == "E") {
      return "删除";
    } else {
      return "溢出";
    }
  };

  // 多单位-表格列
  Goods_unit_columns = [
    {
      title: "包装单位",
      key: "unit_pack",
      customSlot: "unit_pack",
      align: "center",
      width: "70px",
    },
    {
      title: "主条码",
      key: "barcode",
      customSlot: "barcode",
      align: "center",
      width: "140px",
    },
    {
      title: "单位",
      key: "unit",
      customSlot: "unit",
      align: "center",
      width: "60px",
    },
    {
      title: "装量",
      key: "unit_factor",
      customSlot: "unit_factor",
      align: "center",
      width: "80px",
    },
    {
      title: "包装规格",
      key: "unit_size",
      customSlot: "unit_size",
      align: "center",
      ellipsisTooltip: true,
    },
    {
      title: "操作",
      key: "operate",
      customSlot: "operate",
      align: "center",
      width: "110px",
    },
  ];
  // 多条码-表格列
  Goods_barcode_columns = [
    {
      title: "辅助条码",
      key: "barcode",
      customSlot: "barcode",
      align: "center",
      width: "100px",

    },
    {
      title: "操作",
      key: "operate",
      customSlot: "operate",
      align: "center",
      width: "110px",
    },
  ];
} // END
export default GoodsPack;
