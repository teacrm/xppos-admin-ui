// 这里是已经完善的方法存放点-这样架构是为了上层模板代码干净...
class PNDB {
  AA = function (res: any) {
    return res + "OK{src.views.items.listPN.ts}";
  };

  // 状态
  u_statex = (res: string) => {
    if (res == "ds") {
      return "未定义";
    } else if (res == "normal") {
      return "正常";
    } else if (res == "stop") {
      return '<span style="color: #FF6C37;">停用</span>';
    } else {
      return "--";
    }
  };
  // 类型
  u_typex = (res: string) => {
    if (res == "ds") {
      return "未定义";
    } else if (res == "user") {
      return "普通";
    } else if (res == "admin") {
      return "管理";  
     } else if (res == "agent") {
        return "代理";
    } else {
      return "--";
    }
  };
  // 表格列配置
  columns = [
    { title: "序号", width: "60px", type: "number" },
    {title: "行号",width: "60px",customSlot: "index2",key: "index2",align: "center",},
    {title: "主键",width: "80px",customSlot: "id",key: "id",sort: "desc",align: "center",},
    { title: "数据编号", width: "250px", customSlot: "dbid", key: "dbid", sort: "desc" ,align: "center",},

    { title: "名称", width: "210px", key: "us_name", sort: "desc" ,align: "center",},
    {title: "类型",width: "80px",customSlot: "u_type",key: "u_type",sort: "desc",align: "center",},
    {title: "状态",width: "80px",customSlot: "u_state",key: "u_state",sort: "desc",align: "center",},

    { title: "登录时间", width: "150px", key: "login_time", sort: "desc",align: "center", },
    { title: "新建时间", width: "150px", key: "time_new", sort: "desc" ,align: "center",},
    { title: "修改时间", width: "150px", key: "time_up", sort: "desc" ,align: "center",},
    { title: "登录帐号", width: "210px", key: "sn_uid", sort: "desc" ,align: "center",},
    { title: "登录手机", width: "210px", key: "sn_mobile", sort: "desc",align: "center", },
    { title: "登录邮箱", width: "210px", key: "sn_email", sort: "desc",align: "center", },
    { title: "登录工号", width: "210px", key: "sn_job", sort: "desc" ,align: "center",},
    { title: "自设备注", width: "150px", key: "u_txt", sort: "desc" ,align: "center",ellipsisTooltip:"true"},
    {title: "操作",width: "54px",customSlot: "operator",key: "operator",fixed: "right",},
  ];
}
export default PNDB;
