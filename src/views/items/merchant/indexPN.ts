class MerchantPN {
  columns = [
    { title: "序号", width: "60px", type: "number" },
    {
      title: "行号",
      width: "60px",
      customSlot: "index1",
      key: "index1",
      sort: "desc",
      align: "center",
    },
    {
      title: "货商名称",
      width: "210px",
      key: "name",
      sort: "desc",
      align: "center",
    },
    {
      title: "负责人",
      width: "80px",
      customSlot: "principal",
      key: "principal",
      sort: "desc",
      align: "center",
    },
    {
      title: "电话",
      width: "120px",
      key: "tel",
      align: "center",
    },
    {
      title: "手机",
      width: "120px",
      key: "mobile",
      align: "center",
    },
    {
      title: "传真",
      width: "120px",
      key: "fax",
      align: "center",
    },
    {
      title: "电子邮箱",
      width: "120px",
      key: "email",
      align: "center",
    },
    {
      title: "开户银行",
      width: "180px",
      key: "bank_acctno",
      align: "center",
    },
    {
      title: "开户银行",
      width: "180px",
      key: "bank_acctno",
      align: "center",
    },
    {
      title: "银行账号",
      width: "220px",
      key: "bank_no",
      align: "center",
    },
    {
      title: "地址",
      width: "220px",
      key: "addr",
      align: "center",
    },
    {
      title: "备注",
      width: "120px",
      key: "memo",
      align: "center",
    },
    {
      title: "操作",
      width: "54px",
      customSlot: "operator",
      key: "operator",
      fixed: "right",
    },
  ]
}

export default MerchantPN;