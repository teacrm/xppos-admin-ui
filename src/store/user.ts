import { defineStore } from 'pinia'
import { menu, permission } from "../api/module/user";

export const useUserStore = defineStore({
  id: 'user',
  state: () => {
    return {
      token: '',
      userInfo: {},
      permissions: [],
      menus: [],
    }
  },
  actions: {
    async loadMenus(){
      const { Tree, code } = await menu();
      if (code == 200) {
        this.menus = Tree.txt_json;
      }
    },
    async loadPermissions(){
      // const { data, code } = await permission();
      // if(code == 200) {
      //   this.permissions = data;
      // }
    }
  },
  persist: {
    storage: localStorage,
    paths: ['token', 'userInfo', 'permissions', 'menus' ],
  }
})