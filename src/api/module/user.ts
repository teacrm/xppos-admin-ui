import Http from "../http";

export const menu = function () {
  return Http.get("/Menu/ListJSON");
};

export const permission = function () {
  return Http.get("/user/permission");
};
////////////////////////
export const login = function (Form: any) {
  // console.log("登录方法{src/api/module/user.ts}", Form);
  return Http.post("/login", Form);
};
export const logout = function () {
  // console.log("退出登录方法{src/api/module/user.ts}");
  return Http.post("/login/logout");
};
export const userList = function (Form: any) {
  // console.log("用户列表：{src/api/module/user.ts}");
  return Http.post("/user/list", Form);
};
export const userUP = function (Form: any) {
  // console.log("用户修改：{src/api/module/user.ts}");
  return Http.post("/user/up", Form);
};
export const userNEW = function (Form: any) {
  // console.log("用户增加：{src/api/module/user.ts}");
  return Http.post("/user/new", Form);
};

export const cardlist = function (Form: any) {
  // console.log("卡片列表：{src/api/module/user.ts}");
  return Http.post("/card/list", Form);
};
export const cardUP = function (Form: any) {
  // console.log("卡片修改：{src/api/module/card.ts}");
  return Http.post("/card/up", Form);
};
export const cardNEW = function (Form: any) {
  // console.log("卡片增加：{src/api/module/card.ts}");
  return Http.post("/card/new", Form);
};

export const GoodsClsList = function (Form: any) {
  return Http.post("/GoodsCls/List", Form); // console.log("商品类别列表：{src/api/module/user.ts}");
};
export const GoodsClsListJSON = function (Form: any) {
  return Http.post("/GoodsCls/ListJSON", Form); // console.log("商品类别列表：{src/api/module/user.ts}");
};
export const GoodsClsUP = function (Form: any) {
  return Http.post("/GoodsCls/up", Form); // console.log("商品类别修改{src/api/module/user.ts}");
};
export const GoodsClsNEW = function (Form: any) {
  return Http.post("/GoodsCls/new", Form); // console.log("商品类别新增{src/api/module/user.ts}");
};

export const GoodsList = function (Form: any) {
  return Http.post("/Goods/list", Form); // console.log("商品档案列表{src/api/module/user.ts}");
};
export const GoodsNEW = function (Form: any) {
  return Http.post("/Goods/new", Form); // console.log("商品档案新增{src/api/module/user.ts}");
};
export const GoodsUP = function (Form: any) {
  return Http.post("/Goods/up", Form); // console.log("商品档案修改{src/api/module/user.ts}");
};
export const GoodsID = function (Form: any) {
  return Http.post("/Goods/id", Form); // console.log("单条商品档案{src/api/module/user.ts}");
};
export const GoodsX = function (Form: any) {
  return Http.post("/Goods/GoodsX", Form); // console.log("单条商品档案2{src/api/module/user.ts}");
};
export const BarcodeNew = function (Form: any) {
  return Http.post("/Goods/barcodenew", Form); // console.log("单条商品档案辅助条码新增{src/api/module/user.ts}");
};
export const GoodsAutoNumber = function (Form: any) {
  return Http.post("/Goods/GoodsAutoNumber", Form); // console.log("获取商品档案自动编号{src/api/module/user.ts}");
};

export const OrderBuyerNEW = function (Form: any) {
  return Http.post("/OrderBuyer/new", Form); // console.log("采购单据-新增{src/api/module/user.ts}");
};
export const OrderBuyerUP = function (Form: any) {
  return Http.post("/OrderBuyer/up", Form); // console.log("采购单据-修改{src/api/module/user.ts}");
};
export const OrderBuyerID = function (Form: any) {
  return Http.post("/OrderBuyer/id", Form); // console.log("采购单据-查询一条单据{src/api/module/user.ts}");
};

export const OrderBuyerList = function (Form: any) {
  return Http.post("/OrderBuyer/List", Form); // console.log("采购单据-列表{src/api/module/user.ts}");
};
export const OrderBuyerFinish = function (Form: any) {
  return Http.post("/OrderBuyer/Finish", Form); // console.log("采购单据-审核{src/api/module/user.ts}");
};

export const StockList = function (Form: any) {
  return Http.post("/Stock/List", Form); // console.log("商品库存-列表{src/api/module/user.ts}");
};

/** 货商API */
//列表
export const getSupList = function (Form: any) {
  return Http.post("/Sup/List", Form);
};
//新增
export const supNew = function (Form: any) {
  return Http.post("/Sup/new", Form);
};
//修改
export const supUp = function (Form: any) {
  return Http.post("/Sup/up", Form);
};
