import BaseLayout from "../../layouts/BaseLayout.vue";
import Login from "../../views/login/index.vue";

export default [
  {
    path: "/",
    redirect: "/workSpace",
  },
  {
    path: "/login",
    component: Login,
    meta: { title: "登录页面" },
  },
  {
    path: "/workspace",
    redirect: "/workspace/workbench",
    component: BaseLayout,
    meta: { title: "工作空间" },
    children: [
      {
        path: "/workspace/workbench",
        name: "Workbench",
        component: () => import("../../views/workSpace/workbench/index.vue"),
        meta: {
          title: "工作台",
          requireAuth: true,
          affix: true,
          closable: false,
        },
      },
      {
        path: "/workspace/console",
        component: () => import("../../views/workSpace/console/index.vue"),
        meta: { title: "控制台", requireAuth: true },
      },
      {
        path: "/workspace/analysis",
        component: () => import("../../views/workSpace/analysis/index.vue"),
        meta: { title: "分析页", requireAuth: true },
      },
      {
        path: "/workspace/monitor",
        component: () => import("../../views/workSpace/monitor/index.vue"),
        meta: { title: "监控页", requireAuth: true },
      },
    ],
  },
  {
    path: "/error",
    component: BaseLayout,
    meta: { title: "错误页面" },
    children: [
      {
        path: "/error/401",
        component: () => import("../../views/error/401.vue"),
        meta: { title: "401" },
      },
      {
        path: "/error/403",
        component: () => import("../../views/error/403.vue"),
        meta: { title: "403" },
      },
      {
        path: "/error/404",
        component: () => import("../../views/error/404.vue"),
        meta: { title: "404" },
      },
      {
        path: "/error/500",
        component: () => import("../../views/error/500.vue"),
        meta: { title: "500" },
      },
    ],
  },
  {
    path: "/system",
    component: BaseLayout,
    meta: { title: "系统管理" },
    children: [
      {
        path: "/system/user",
        component: () => import("../../views/system/user/index.vue"),
        meta: { title: "用户管理", requireAuth: true },
      },
      {
        path: "/system/role",
        component: () => import("../../views/system/role/index.vue"),
        meta: { title: "角色管理", requireAuth: true },
      },
      {
        path: "/system/menu",
        component: () => import("../../views/system/menu/index.vue"),
        meta: { title: "菜单管理", requireAuth: true },
      },
      {
        path: "/system/organization",
        component: () => import("../../views/system/organization/index.vue"),
        meta: { title: "机构管理", requireAuth: true },
      },
      {
        path: "/system/dictionary",
        component: () => import("../../views/system/dictionary/index.vue"),
        meta: { title: "字典管理", requireAuth: true },
      },
      {
        path: "/system/file",
        component: () => import("../../views/system/file/index.vue"),
        meta: { title: "文件管理", requireAuth: true },
      },
      {
        path: "/system/login",
        component: () => import("../../views/system/login/index.vue"),
        meta: { title: "登录日志", requireAuth: true },
      },
      {
        path: "/system/option",
        component: () => import("../../views/system/option/index.vue"),
        meta: { title: "操作日志", requireAuth: true },
      },
    ],
  },
  {
    path: "/enrollee",
    component: BaseLayout,
    meta: { title: "个人中心" },
    children: [
      {
        path: "/enrollee/profile",
        component: () => import("../../views/enrollee/profile/index.vue"),
        meta: { title: "我的资料", requireAuth: true },
      },
      {
        path: "/enrollee/message",
        component: () => import("../../views/enrollee/message/index.vue"),
        meta: { title: "我的消息", requireAuth: true },
      },
    ],
  },
  {
    path: "/items",
    component: BaseLayout,
    meta: { title: "信息档案" },
    children: [
      {
        path: "/items/user",
        component: () => import("../../views/items/user/index.vue"),
        meta: { title: "账号档案", requireAuth: true },
      },

      {
        path: "/items/goods",
        component: () => import("../../views/items/goods/index.vue"),
        meta: { title: "商品档案", requireAuth: true },
      },
      {
        path: "/items/goods/views/cls",
        component: () => import("../../views/items/goods/views/cls/index.vue"),
        meta: { title: "商品档案", requireAuth: true },
      },
      {
        path: "/buyer/storage",
        component: () => import("../../views/buyer/storage/index.vue"),
        meta: { title: "采购入库", requireAuth: true },
      },
      {
        path: "/stock",
        component: () => import("../../views/stock/index.vue"),
        meta: { title: "商品库存", requireAuth: true },
      },
      {
        path: "/items/merchant",
        component: () => import("../../views/items/merchant/index.vue"),
        meta: { title: "货商信息", requireAuth: true },
      },
    ],
  },
];
