import {App} from 'vue';
import searchForm from './src/searchForm.vue';

export default {
  install(app: App) {
    app.component('SearchForm', searchForm)
  }
};