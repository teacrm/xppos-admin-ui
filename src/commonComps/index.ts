import {App} from 'vue';
import SearchFrom from './SearchFrom';

const all = [SearchFrom]

export default {
  install(app: App) {
    all.map(item => {
      app.use(item)
    })
  }
}